const route = require ('express').Router()
const  { PlayersController , BiodataController, HistoryController, MiddlewareLogin}  = require('../controller')

route.post("/player", PlayersController.createPlayer)
route.get("/", PlayersController.getPlayers)
route.get("/player/:id", PlayersController.getPlayer)
route.post("/player/:id", PlayersController.editPlayer) //diganti post supaya bisa metod pengeditan di form
route.get("/delplayer/:id", PlayersController.deletePlayer) //diganti get dari delete (tidak menggunakan axios)

route.get("/edit/player/:id", PlayersController.editPlayerView)
route.get("/add/player", PlayersController.addPlayerView)
route.delete("/delete/player", PlayersController.deletePlayerView)
route.get("/home", PlayersController.homePage)
route.get("/login", MiddlewareLogin.adminLogin)

route.post("/biodata", BiodataController.createBiodata)
route.get("/biodata", BiodataController.getBiodatas)

route.post("/history", HistoryController.createHistory)
route.get("/history", HistoryController.getHistories)



module.exports = route